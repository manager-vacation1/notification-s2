package com.dharbor.talent.managervacations.notificationsservice.constant;

/**
 * @author Jhonatan Soto
 */
public enum VacationStatus {
    REQUESTED,
    APPROVED,
    DENIED
}
