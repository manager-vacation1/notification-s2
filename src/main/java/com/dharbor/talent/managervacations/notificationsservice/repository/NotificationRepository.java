package com.dharbor.talent.managervacations.notificationsservice.repository;

import com.dharbor.talent.managervacations.notificationsservice.domain.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    List<Notification> findAllByIdManager(Long idManager);

}
