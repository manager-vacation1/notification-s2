package com.dharbor.talent.managervacations.notificationsservice.service;

import com.dharbor.talent.managervacations.notificationsservice.domain.Notification;
import com.dharbor.talent.managervacations.notificationsservice.repository.NotificationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class NotificationServiceImpl implements INotificationService {

    private NotificationRepository notificationRepository;

    @Override
    public Notification save(Notification notification) {
        return notificationRepository.save(notification);
    }

    @Override
    public List<Notification> findAllByIdManager(Long idManager) {
        return notificationRepository.findAllByIdManager(idManager);
    }


}
