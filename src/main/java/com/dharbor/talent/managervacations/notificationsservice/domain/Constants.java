package com.dharbor.talent.managervacations.notificationsservice.domain;

/**
 * @author Josue Jimenez Nina
 */
final class Constants {

    static class NotificationsTable {
        static final String NAME = "notifications_table";

        static class Id {
            static final String NAME = "notifications_id";
        }

        static class IdUser {
            static final String NAME = "notifications_id_user";
        }

        static class IdVacation {
            static final String NAME = "notifications_id_vacation";
        }

        static class IdManager {
            static final String NAME = "notifications_id_manager";
        }

        static class DayOff {
            static final String NAME = "notifications_day_off";
        }
    }
}
