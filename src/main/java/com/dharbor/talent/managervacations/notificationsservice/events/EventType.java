package com.dharbor.talent.managervacations.notificationsservice.events;

/**
 * @author Jhonatan Soto
 */
public enum EventType {
    CREATED,UPDATED,DELETED
}
