package com.dharbor.talent.managervacations.notificationsservice.service;

import com.dharbor.talent.managervacations.notificationsservice.domain.Notification;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
public interface INotificationService {
    Notification save(Notification notification);

    List<Notification> findAllByIdManager(Long idManager);
}
