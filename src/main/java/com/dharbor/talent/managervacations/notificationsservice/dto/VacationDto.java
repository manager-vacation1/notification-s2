package com.dharbor.talent.managervacations.notificationsservice.dto;

import com.dharbor.talent.managervacations.notificationsservice.constant.VacationStatus;
import com.dharbor.talent.managervacations.notificationsservice.constant.VacationType;
import lombok.*;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */


public class VacationDto {

    private Long id;

    private VacationType vacationType;

    private Date dayOff;

    private VacationStatus vacationStatus;

    private Long userId;

    public VacationDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VacationType getVacationType() {
        return vacationType;
    }

    public void setVacationType(VacationType vacationType) {
        this.vacationType = vacationType;
    }

    public Date getDayOff() {
        return dayOff;
    }

    public void setDayOff(Date dayOff) {
        this.dayOff = dayOff;
    }

    public VacationStatus getVacationStatus() {
        return vacationStatus;
    }

    public void setVacationStatus(VacationStatus vacationStatus) {
        this.vacationStatus = vacationStatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public VacationDto(Long id, VacationType vacationType, Date dayOff, VacationStatus vacationStatus, Long userId) {
        this.id = id;
        this.vacationType = vacationType;
        this.dayOff = dayOff;
        this.vacationStatus = vacationStatus;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "VacationDto{" +
                "id=" + id +
                ", vacationType=" + vacationType +
                ", dayOff=" + dayOff +
                ", vacationStatus=" + vacationStatus +
                ", userId=" + userId +
                '}';
    }
}
