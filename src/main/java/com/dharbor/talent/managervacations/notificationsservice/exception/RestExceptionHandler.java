package com.dharbor.talent.managervacations.notificationsservice.exception;

import com.dharbor.talent.managervacations.notificationsservice.constant.StatusCode;
import com.dharbor.talent.managervacations.notificationsservice.domain.CommonResponseDomain;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Jhonatan Soto
 */
@RestControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<CommonResponseDomain> unknownException(Exception ex) {
        return new ResponseEntity<>(new CommonResponseDomain(StatusCode.INTERNAL_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
