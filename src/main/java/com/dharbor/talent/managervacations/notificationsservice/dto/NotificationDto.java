package com.dharbor.talent.managervacations.notificationsservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class NotificationDto {

    private Long id;

    private Long idUser;

    private Long idVacation;

    private Long idManager;

    private Date dayOff;

}
