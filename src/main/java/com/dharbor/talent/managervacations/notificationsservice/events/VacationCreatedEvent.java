package com.dharbor.talent.managervacations.notificationsservice.events;

import com.dharbor.talent.managervacations.notificationsservice.dto.VacationDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Jhonatan Soto
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VacationCreatedEvent extends Event<VacationDto> {
}
