package com.dharbor.talent.managervacations.notificationsservice.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author Jhonatan Soto
 */
@Component
public class ObjectMapping {
    @Bean
    public ObjectMapper verifications() {
        return new ObjectMapper();
    }
}
