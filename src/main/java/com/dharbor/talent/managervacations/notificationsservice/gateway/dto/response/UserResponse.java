package com.dharbor.talent.managervacations.notificationsservice.gateway.dto.response;

import com.dharbor.talent.managervacations.notificationsservice.domain.CommonResponseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class UserResponse extends CommonResponseDomain {

    private UserDto userDto;

}
