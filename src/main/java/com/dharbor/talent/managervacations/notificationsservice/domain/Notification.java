package com.dharbor.talent.managervacations.notificationsservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@Entity
@Table(name = Constants.NotificationsTable.NAME)
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Constants.NotificationsTable.Id.NAME)
    private Long id;

    @Column(name = Constants.NotificationsTable.IdUser.NAME)
    private Long idUser;

    @Column(name = Constants.NotificationsTable.IdVacation.NAME)
    private Long idVacation;

    @Column(name = Constants.NotificationsTable.IdManager.NAME)
    private Long idManager;

    @Temporal(TemporalType.DATE)
    @Column(name = Constants.NotificationsTable.DayOff.NAME)
    private Date dayOff;

}
