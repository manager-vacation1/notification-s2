package com.dharbor.talent.managervacations.notificationsservice.gateway.dto.response;

import com.dharbor.talent.managervacations.notificationsservice.constant.UserType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class UserDto {

    private Long id;

    private String email;

    private UserType userType;

    private Date createdDate;

}
