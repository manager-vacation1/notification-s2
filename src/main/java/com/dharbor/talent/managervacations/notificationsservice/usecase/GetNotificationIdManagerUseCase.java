package com.dharbor.talent.managervacations.notificationsservice.usecase;

import com.dharbor.talent.managervacations.notificationsservice.dto.NotificationDto;
import com.dharbor.talent.managervacations.notificationsservice.dto.response.NotificationResponse;
import com.dharbor.talent.managervacations.notificationsservice.mapper.NotificationStructMapper;
import com.dharbor.talent.managervacations.notificationsservice.service.INotificationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetNotificationIdManagerUseCase {

    private INotificationService iNotificationService;

    private NotificationStructMapper notificationStructMapper;

    public NotificationResponse execute(Long id) {
        List<NotificationDto> notificationDtos = notificationStructMapper
                .notificationListToNotificationListDto(iNotificationService.findAllByIdManager(id));
        return new NotificationResponse(notificationDtos);
    }

}
