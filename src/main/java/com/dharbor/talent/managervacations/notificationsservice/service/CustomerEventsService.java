package com.dharbor.talent.managervacations.notificationsservice.service;

import com.dharbor.talent.managervacations.notificationsservice.domain.Notification;
import com.dharbor.talent.managervacations.notificationsservice.dto.VacationDto;
import com.dharbor.talent.managervacations.notificationsservice.gateway.SecurityClient;
import com.dharbor.talent.managervacations.notificationsservice.gateway.dto.response.UserResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author Jhonatan Soto
 */
@Slf4j
@Component
@AllArgsConstructor
public class CustomerEventsService {

    private ObjectMapper objectMapper;

    private SecurityClient securityClient;

    private INotificationService iNotificationService;

    @KafkaListener(topics = "${topic.customer.name:test3}", groupId = "grupo1", containerFactory = "greetingKafkaListenerContainerFactory")
    public void consumer(String vacationNotification) throws JsonProcessingException {
        VacationDto vacationDto = objectMapper.readValue(vacationNotification, VacationDto.class);
        UserResponse userResponse = securityClient.getManagerByUserId(vacationDto.getUserId());
        Notification notification = new Notification();
        notification.setIdUser(vacationDto.getUserId());
        notification.setIdVacation(vacationDto.getId());
        notification.setIdManager(userResponse.getUserDto().getId());
        notification.setDayOff(vacationDto.getDayOff());
        iNotificationService.save(notification);
    }
}
